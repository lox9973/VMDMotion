using System.Collections.Generic;
using UnityEngine;

namespace VMDMotion {
public class Skeleton {
	public class Bone {
		public Standard.BoneNames name;
		public Transform  transform;
		public Vector3    localPosition0;

		public Transform  target;
		public Vector3    targetPosition;
		public Quaternion targetRotation;

		public bool ikEnabled;
		public bool ikCopyQ;

		public Bone(Standard.BoneNames name, Transform parent, Transform source, Transform target) {
			this.name = name;
			transform = new GameObject(name.ToString()).transform;
			transform.SetParent(parent, false);

			if(source)
				transform.position = source.position;
			localPosition0 = transform.localPosition;

			if(target) {
				this.target = target;
				targetPosition = transform.InverseTransformPoint(target.position);
				targetRotation = Quaternion.Inverse(transform.rotation) * target.rotation;
			}
		}

		public void ApplyTarget() {
			if(target)
				target.SetPositionAndRotation(
					transform.TransformPoint(targetPosition), transform.rotation * targetRotation);
		}
	}
	public Animator animator;
	public Bone[] bones;
	public Transform root;

	public Skeleton(Animator animator, Dictionary<Standard.BoneNames, Transform> sourceOverrides=null) {
		this.animator = animator;
		
		root = new GameObject("MMDArmature").transform;
		root.SetParent(animator.transform, false);
		root.localEulerAngles = new Vector3(0,180,0); // mmd => unity coord system

		bones = new Bone[System.Enum.GetNames(typeof(Standard.BoneNames)).Length];
		foreach(var template in Standard.Bones) {
			var parentTransform = template.Parent == null ? root : bones[(int)template.Parent.Value].transform;
			var positionTransform = sourceOverrides.ContainsKey(template.Name) ? sourceOverrides[template.Name]
					: template.Source == null ? null : animator.GetBoneTransform(template.Source.Value);
			var target = template.Target == null ? null : animator.GetBoneTransform(template.Target.Value);
			bones[(int)template.Name] = new Bone(template.Name, parentTransform, positionTransform, target);
		}
		for(int i=0; i<bones.Length; i++)
			if(bones[i] == null)
				bones[i] = new Bone((Standard.BoneNames)i, root, null, null);
	}
	public void ApplyTargets() {
		foreach(var bone in bones)
			bone.ApplyTarget();
	}
	public void ApplyConstraints(bool ikEnabled=true, bool ikCopyQ=false) {
		foreach(var cons in Standard.Constraints)
			switch(cons) {
			case Standard.RotAdd rotAdd:
				ApplyRotAdd(bones[(int)rotAdd.Target].transform, bones[(int)rotAdd.Source].transform, rotAdd.Minus);
				break;
			case Standard.LimbIK limbIK:
			if(ikEnabled)
				ApplyLimbIK(bones[(int)limbIK.Target0].transform, bones[(int)limbIK.Target1].transform,
					bones[(int)limbIK.Target2].transform, bones[(int)limbIK.Source], ikCopyQ);
				break;
			case Standard.LookAt lookAt:
			if(ikEnabled)
				ApplyLookAt(bones[(int)lookAt.Target0].transform, bones[(int)lookAt.Target1].transform,
					lookAt.Source0 == null ? null : bones[(int)lookAt.Source0.Value], bones[(int)lookAt.Source1], ikCopyQ);
				break;
			}
	}
	static void ApplyRotAdd(Transform target, Transform source, bool minus) {
		if(minus) // remove source.localRotation from rotation hierarchy
			target.rotation = source.parent.rotation * Quaternion.Inverse(source.rotation) * target.rotation;
		else
			target.localRotation = source.localRotation * target.localRotation;
	}
	static void ApplyLimbIK(Transform upperLeg, Transform lowerLeg, Transform foot, Bone footIK, bool copyQ) {
		if(!footIK.ikEnabled)
			return;
		var localTarget = upperLeg.InverseTransformPoint(footIK.transform.position);
		var bend = -CalcBend(lowerLeg.localPosition, foot.localPosition, localTarget.magnitude);
		lowerLeg.localRotation = new Quaternion(Mathf.Sin(bend/2), 0, 0, Mathf.Cos(bend/2));
		upperLeg.localRotation *= Quaternion.FromToRotation(
			upperLeg.InverseTransformPoint(foot.position), localTarget);
		if(footIK.ikCopyQ && copyQ)
			foot.rotation = footIK.transform.rotation;
	}
	static void ApplyLookAt(Transform foot, Transform toe, Bone footIK, Bone toeIK, bool copyQ) {
		if(footIK != null && !footIK.ikEnabled)
			return; // disabling toeIK when footIK is off works best in practice
		if(!toeIK.ikEnabled)
			return;
		foot.rotation *= Quaternion.FromToRotation(toe.localPosition,
			foot.InverseTransformPoint(toeIK.transform.position));
		if(toeIK.ikCopyQ && copyQ)
			toe.rotation = toeIK.transform.rotation;
	}
	static float CalcBend(Vector3 v0, Vector3 v1, float dist) {
		// find x s.t. (v0 + rotateX(x) * v1).magnitude == dist
		var u0 = new Vector2(v0.y, v0.z);
		var u1 = new Vector2(v1.y, v1.z);
		var dot = (dist*dist - v0.sqrMagnitude - v1.sqrMagnitude)/2 - v0.x*v1.x;
		// find rot s.t. Dot(u0, rot * u1) == dot
		u1 = new Vector2(u0.x*u1.x + u0.y*u1.y, u0.x*u1.y - u1.x*u0.y);
		return Mathf.Max(0f, Mathf.Acos(Mathf.Clamp(dot/u1.magnitude, -1, 1)) - Mathf.Atan2(u1.y, u1.x));
	}
}
}