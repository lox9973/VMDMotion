using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.HumanBodyBones;

namespace VMDMotion {
using static Standard.BoneNames;
public static class Standard {
	public enum BoneNames {
		全ての親, センター, グルーブ,
		左足IK親, 左足ＩＫ, 左つま先ＩＫ,
		右足IK親, 右足ＩＫ, 右つま先ＩＫ,
		腰, 下半身, 上半身, 上半身2, 首, 頭,
		左目, 右目, 両目,
		左肩P, 左肩, 左肩C, 左腕, 左腕捩, 左ひじ, 左手捩, 左手首,
		右肩P, 右肩, 右肩C, 右腕, 右腕捩, 右ひじ, 右手捩, 右手首,
		左足, 左ひざ, 左足首, 左つま先,
		右足, 右ひざ, 右足首, 右つま先,
		左足D, 左ひざD, 左足首D, 左足先EX,
		右足D, 右ひざD, 右足首D, 右足先EX,
		左親指０, 左親指１, 左親指２, 左人指１, 左人指２, 左人指３,
		左中指１, 左中指２, 左中指３, 左薬指１, 左薬指２, 左薬指３, 左小指１, 左小指２, 左小指３,
		右親指０, 右親指１, 右親指２, 右人指１, 右人指２, 右人指３,
		右中指１, 右中指２, 右中指３, 右薬指１, 右薬指２, 右薬指３, 右小指１, 右小指２, 右小指３,
	}
	public static BoneNames[] MirrorBoneNames = System.Enum.GetNames(typeof(BoneNames))
		.Select(name => (BoneNames)System.Enum.Parse(typeof(BoneNames), name[0] == '左' ? "右" + name.Substring(1)
			: name[0] == '右' ? "左" + name.Substring(1) : name)).ToArray();
	public static string FixBoneName(string name) {
		return name.Replace("捩れ", "捩").Replace("捻", "捩");
	}

	public class Bone {
		public BoneNames Name;
		public BoneNames? Parent;
		public HumanBodyBones? Source;
		public HumanBodyBones? Target;
	}
	public abstract class Constraint { }
	public class RotAdd : Constraint {
		public BoneNames Target;
		public BoneNames Source;
		public bool Minus;
	}
	public class LimbIK : Constraint {
		public BoneNames Source;
		public BoneNames Target0, Target1, Target2;
	}
	public class LookAt : Constraint {
		public BoneNames? Source0;
		public BoneNames Source1;
		public BoneNames Target0, Target1;
	}
	public static Bone[] Bones = new [] {
		new Bone{Name=全ての親},
		new Bone{Name=センター,	Parent=全ての親,	Source=Spine},
		new Bone{Name=グルーブ,	Parent=センター},
		new Bone{Name=腰,		Parent=グルーブ},
		new Bone{Name=下半身,	Parent=腰,		Source=Spine, Target=Hips},
		new Bone{Name=上半身,	Parent=腰,		Source=Spine, Target=Spine},
		new Bone{Name=上半身2,	Parent=上半身,	Source=Chest, Target=Chest},
		new Bone{Name=首,		Parent=上半身2,	Source=Neck,  Target=Neck},
		new Bone{Name=頭,		Parent=首,		Source=Head,  Target=Head},
		new Bone{Name=両目,		Parent=頭},
		new Bone{Name=左目,		Parent=頭,		Source=LeftEye,  Target=LeftEye},
		new Bone{Name=右目,		Parent=頭,		Source=RightEye, Target=RightEye},

		new Bone{Name=左肩P,		Parent=上半身2,	Source=LeftShoulder},
		new Bone{Name=左肩,		Parent=左肩P,	Target=LeftShoulder},
		new Bone{Name=左肩C,		Parent=左肩,		Source=LeftUpperArm},
		new Bone{Name=左腕,		Parent=左肩C,	Target=LeftUpperArm},
		new Bone{Name=左腕捩,	Parent=左腕,		Source=LeftLowerArm},
		new Bone{Name=左ひじ,		Parent=左腕捩,	Target=LeftLowerArm},
		new Bone{Name=左手捩,	Parent=左ひじ,	Source=LeftHand},
		new Bone{Name=左手首,	Parent=左手捩,	Target=LeftHand},

		new Bone{Name=右肩P,		Parent=上半身2,	Source=RightShoulder},
		new Bone{Name=右肩,		Parent=右肩P,	Target=RightShoulder},
		new Bone{Name=右肩C,		Parent=右肩,		Source=RightUpperArm},
		new Bone{Name=右腕,		Parent=右肩C,	Target=RightUpperArm},
		new Bone{Name=右腕捩,	Parent=右腕,		Source=RightLowerArm},
		new Bone{Name=右ひじ,		Parent=右腕捩,	Target=RightLowerArm},
		new Bone{Name=右手捩,	Parent=右ひじ,	Source=RightHand},
		new Bone{Name=右手首,	Parent=右手捩,	Target=RightHand},

		new Bone{Name=左足IK親,		Parent=全ての親,	Source=LeftFoot},
		new Bone{Name=左足ＩＫ,		Parent=左足IK親},
		new Bone{Name=左つま先ＩＫ,	Parent=左足ＩＫ,	Source=LeftToes},
		new Bone{Name=左足,		Parent=下半身,		Source=LeftUpperLeg},
		new Bone{Name=左足D,		Parent=下半身,		Source=LeftUpperLeg, Target=LeftUpperLeg},
		new Bone{Name=左ひざ,		Parent=左足,			Source=LeftLowerLeg},
		new Bone{Name=左ひざD,	Parent=左足D,		Source=LeftLowerLeg, Target=LeftLowerLeg},
		new Bone{Name=左足首,	Parent=左ひざ,		Source=LeftFoot},
		new Bone{Name=左足首D,	Parent=左ひざD,		Source=LeftFoot,     Target=LeftFoot},
		new Bone{Name=左つま先,	Parent=左足首,		Source=LeftToes},
		new Bone{Name=左足先EX,	Parent=左足首D,		Source=LeftToes,     Target=LeftToes},

		new Bone{Name=右足IK親,		Parent=全ての親,	Source=RightFoot},
		new Bone{Name=右足ＩＫ,		Parent=右足IK親},
		new Bone{Name=右つま先ＩＫ,	Parent=右足ＩＫ,	Source=RightToes},
		new Bone{Name=右足,		Parent=下半身,		Source=RightUpperLeg},
		new Bone{Name=右足D,		Parent=下半身,		Source=RightUpperLeg, Target=RightUpperLeg},
		new Bone{Name=右ひざ,		Parent=右足,			Source=RightLowerLeg},
		new Bone{Name=右ひざD,	Parent=右足D,		Source=RightLowerLeg, Target=RightLowerLeg},
		new Bone{Name=右足首,	Parent=右ひざ,		Source=RightFoot},
		new Bone{Name=右足首D,	Parent=右ひざD,		Source=RightFoot,     Target=RightFoot},
		new Bone{Name=右つま先,	Parent=右足首,		Source=RightToes},
		new Bone{Name=右足先EX,	Parent=右足首D,		Source=RightToes,     Target=RightToes},
	}.Concat(new [] {
		(左手首, 左親指０, 左親指１, 左親指２, LeftThumbProximal,  LeftThumbIntermediate,  LeftThumbDistal),
		(左手首, 左人指１, 左人指２, 左人指３, LeftIndexProximal,  LeftIndexIntermediate,  LeftIndexDistal),
		(左手首, 左中指１, 左中指２, 左中指３, LeftMiddleProximal, LeftMiddleIntermediate, LeftMiddleDistal),
		(左手首, 左薬指１, 左薬指２, 左薬指３, LeftRingProximal,   LeftRingIntermediate,   LeftRingDistal),
		(左手首, 左小指１, 左小指２, 左小指３, LeftLittleProximal, LeftLittleIntermediate, LeftLittleDistal),

		(右手首, 右親指０, 右親指１, 右親指２, RightThumbProximal,  RightThumbIntermediate,  RightThumbDistal),
		(右手首, 右人指１, 右人指２, 右人指３, RightIndexProximal,  RightIndexIntermediate,  RightIndexDistal),
		(右手首, 右中指１, 右中指２, 右中指３, RightMiddleProximal, RightMiddleIntermediate, RightMiddleDistal),
		(右手首, 右薬指１, 右薬指２, 右薬指３, RightRingProximal,   RightRingIntermediate,   RightRingDistal),
		(右手首, 右小指１, 右小指２, 右小指３, RightLittleProximal, RightLittleIntermediate, RightLittleDistal),
	}.SelectMany(x => new [] {
		new Bone{Name=x.Item2, Parent=x.Item1, Source=x.Item5, Target=x.Item5},
		new Bone{Name=x.Item3, Parent=x.Item2, Source=x.Item6, Target=x.Item6},
		new Bone{Name=x.Item4, Parent=x.Item3, Source=x.Item7, Target=x.Item7},
	})).ToArray();
	public static Constraint[] Constraints = new Constraint[] {
		new RotAdd{Target=左目, Source=両目},
		new RotAdd{Target=右目, Source=両目},
		new RotAdd{Target=左肩C, Source=左肩P, Minus=true},
		new RotAdd{Target=右肩C, Source=右肩P, Minus=true},

		new LimbIK{Source=左足ＩＫ,  Target0=左足, Target1=左ひざ, Target2=左足首},
		new LookAt{Source0=左足ＩＫ, Source1=左つま先ＩＫ, Target0=左足首, Target1=左つま先},
		new RotAdd{Target=左足D,   Source=左足},
		new RotAdd{Target=左ひざD,  Source=左ひざ},
		new RotAdd{Target=左足首D, Source=左足首},

		new LimbIK{Source=右足ＩＫ,  Target0=右足, Target1=右ひざ, Target2=右足首},
		new LookAt{Source0=右足ＩＫ, Source1=右つま先ＩＫ, Target0=右足首, Target1=右つま先},
		new RotAdd{Target=右足D,   Source=右足},
		new RotAdd{Target=右ひざD,  Source=右ひざ},
		new RotAdd{Target=右足首D, Source=右足首},
	};
}
}
