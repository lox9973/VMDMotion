using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace VMDMotion {
public class VMD {
	public class BezierInterpolator {
		public float X0, Y0, X1, Y1; // degree-3 bezier curve: (0,0),(X0,Y0),(X1,Y1),(1,1)
		public float InverseLerp(float a, float b, float x) {
			x = Mathf.InverseLerp(a, b, x);
			if(X0 == Y0 && X1 == Y1)
				return x; // shortcut for linear interp
			float t = 0.5f;
			for(float p = 0.25f; p > 1e-6f; p *= 0.5f) // binary search x(t)
				t -= p * Mathf.Sign(t*(3*(1-t)*(X0 + t*(X1-X0)) + t*t) - x);
			return t*(3*(1-t)*(Y0 + t*(Y1-Y0)) + t*t); // evaluate y(t)
		}
		public bool IsLinear => X0 == Y0 && X1 == Y1;
		public override string ToString() => new Vector4(X0,Y0,X1,Y1).ToString();
	}
	public class BoneKeyframe {
		public string Name;
		public int FrameNumber;
		public Vector3 Position;
		public Quaternion Rotation;
		public (BezierInterpolator X, BezierInterpolator Y, BezierInterpolator Z,
				BezierInterpolator Rotation) Interp;

		public BoneKeyframe() { }
		public BoneKeyframe(BinaryReader binaryReader) { Read(binaryReader); }
		public void Read(BinaryReader binaryReader) {
			Name = VMD.ReadString(binaryReader, 15);
			FrameNumber = binaryReader.ReadInt32();
			Position = VMD.ReadVector3(binaryReader);
			Rotation = VMD.ReadQuaternion(binaryReader);
			Interp = (VMD.ReadBezierXYXY(binaryReader, 4), VMD.ReadBezierXYXY(binaryReader, 4),
					VMD.ReadBezierXYXY(binaryReader, 4), VMD.ReadBezierXYXY(binaryReader, 4));
		}
	}
	public class FaceKeyframe {
		public string Name;
		public int FrameNumber;
		public float Weight;

		public FaceKeyframe() { }
		public FaceKeyframe(BinaryReader binaryReader) { Read(binaryReader); }
		public void Read(BinaryReader binaryReader) {
			Name = VMD.ReadString(binaryReader, 15);
			FrameNumber = binaryReader.ReadInt32();
			Weight = binaryReader.ReadSingle();
		}
	}
	public class CameraKeyframe {
		public int FrameNumber;
		public float Distance;
		public Vector3 Position;
		public Vector3 EulerAngles; // in degree, following Unity convention
		public (BezierInterpolator X, BezierInterpolator Y, BezierInterpolator Z,
				BezierInterpolator EulerAngles,
				BezierInterpolator Distance, BezierInterpolator FieldOfView) Interp;
		public float FieldOfView;
		public bool Orthographic;

		public CameraKeyframe() { }
		public CameraKeyframe(BinaryReader binaryReader) { Read(binaryReader); }
		public void Read(BinaryReader binaryReader) {
			FrameNumber = binaryReader.ReadInt32();
			Distance = binaryReader.ReadSingle();
			Position = VMD.ReadVector3(binaryReader);
			EulerAngles = VMD.ReadVector3(binaryReader) * -Mathf.Rad2Deg;
			Interp = (VMD.ReadBezierXXYY(binaryReader, 1), VMD.ReadBezierXXYY(binaryReader, 1),
					VMD.ReadBezierXXYY(binaryReader, 1), VMD.ReadBezierXXYY(binaryReader, 1),
					VMD.ReadBezierXXYY(binaryReader, 1), VMD.ReadBezierXXYY(binaryReader, 1));
			FieldOfView = (float)binaryReader.ReadInt32();
			Orthographic = binaryReader.ReadByte() != 0;
		}
	}
	public class LightKeyframe {
		public int FrameNumber;
		public Color LightColor;
		public Vector3 Position;

		public LightKeyframe() { }
		public LightKeyframe(BinaryReader binaryReader) { Read(binaryReader); }
		public void Read(BinaryReader binaryReader) {
			FrameNumber = binaryReader.ReadInt32();
			LightColor = new Color(binaryReader.ReadSingle(), binaryReader.ReadSingle(), binaryReader.ReadSingle(), 1);
			Position = VMD.ReadVector3(binaryReader);
		}
	}
	public class SelfShadowKeyframe {
		public int FrameNumber;
		public byte Type;
		public float Distance;

		public SelfShadowKeyframe() { }
		public SelfShadowKeyframe(BinaryReader binaryReader) { Read(binaryReader); }
		public void Read(BinaryReader binaryReader)  {
			FrameNumber = binaryReader.ReadInt32();
			Type = binaryReader.ReadByte();
			Distance = binaryReader.ReadSingle();
		}
	}
	public class IKKeyframe {
		public int FrameNumber;
		public bool Display;
		public (string, bool)[] IKEnabled;

		public IKKeyframe() { }
		public IKKeyframe(BinaryReader binaryReader) { Read(binaryReader); }
		public void Read(BinaryReader binaryReader) {
			FrameNumber = binaryReader.ReadInt32();
			Display = binaryReader.ReadByte() != 0;
			IKEnabled = new (string, bool)[binaryReader.ReadInt32()];
			for(int i = 0; i < IKEnabled.Length; i++)
				IKEnabled[i] = (VMD.ReadString(binaryReader, 20), binaryReader.ReadByte() != 0);
		}
	}

	public string Version, Name;
	public List<BoneKeyframe> BoneKeyframes = new List<BoneKeyframe>();
	public List<FaceKeyframe> FaceKeyframes = new List<FaceKeyframe>();
	public List<CameraKeyframe> CameraKeyframes = new List<CameraKeyframe>();
	public List<LightKeyframe> LightKeyframes = new List<LightKeyframe>();
	public List<SelfShadowKeyframe> SelfShadowKeyframes = new List<SelfShadowKeyframe>();
	public List<IKKeyframe> IKKeyframes = new List<IKKeyframe>();

	public VMD(string path) { Read(path); }
	private void Read(string path) {
		using(var fileStream = File.OpenRead(path))
		using(var binaryReader = new BinaryReader(fileStream))
			Read(binaryReader);
	}
	private void Read(BinaryReader binaryReader) {
		Version = VMD.ReadString(binaryReader, 30);
		Name = VMD.ReadString(binaryReader, 20);
		Debug.Log($"{Name} ({Version})");
		if(!Version.StartsWith("Vocaloid Motion Data")) {
			Debug.LogError("Invalid vmd file");
			return;
		}

		var boneFrameCount = binaryReader.ReadInt32();
		BoneKeyframes.Clear();
		for(int i = 0; i < boneFrameCount; i++)
			BoneKeyframes.Add(new BoneKeyframe(binaryReader));

		if(binaryReader.BaseStream.Position == binaryReader.BaseStream.Length)
			return;

		var faceFrameCount = binaryReader.ReadInt32();
		Debug.Log($"faceFrameCount={faceFrameCount}");
		FaceKeyframes.Clear();
		for(int i = 0; i < faceFrameCount; i++)
			FaceKeyframes.Add(new FaceKeyframe(binaryReader));
		
		try {
			if(binaryReader.BaseStream.Position == binaryReader.BaseStream.Length)
				return;

			var cameraFrameCount = binaryReader.ReadInt32();
			if(cameraFrameCount != 0)
				Debug.Log($"cameraFrameCount={cameraFrameCount}");
			CameraKeyframes.Clear();
			for(int i = 0; i < cameraFrameCount; i++)
				CameraKeyframes.Add(new CameraKeyframe(binaryReader));
			
			if(binaryReader.BaseStream.Position == binaryReader.BaseStream.Length)
				return;

			var lightFrameCount = binaryReader.ReadInt32();
			if(lightFrameCount != 0)
				Debug.Log($"lightFrameCount={lightFrameCount}");
			LightKeyframes.Clear();
			for(int i = 0; i < lightFrameCount; i++)
				LightKeyframes.Add(new LightKeyframe(binaryReader));
			
			if(binaryReader.BaseStream.Position == binaryReader.BaseStream.Length)
				return;

			var selfShadowFrameCount = binaryReader.ReadInt32();
			if(selfShadowFrameCount != 0)
				Debug.Log($"selfShadowFrameCount={selfShadowFrameCount}");
			SelfShadowKeyframes.Clear();
			for(int i = 0; i < selfShadowFrameCount; i++)
				SelfShadowKeyframes.Add(new SelfShadowKeyframe(binaryReader));
			
			if(binaryReader.BaseStream.Position == binaryReader.BaseStream.Length)
				return;

			var ikFrameCount = binaryReader.ReadInt32();
			Debug.Log($"ikFrameCount={ikFrameCount}");
			IKKeyframes.Clear();
			for(int i = 0; i < ikFrameCount; i++)
				IKKeyframes.Add(new IKKeyframe(binaryReader));
		} catch {
			Debug.LogWarning("Error in parsing VMD");
		}
	}
	static Vector3 ReadVector3(BinaryReader binaryReader) {
		return new Vector3(
			binaryReader.ReadSingle(), binaryReader.ReadSingle(), binaryReader.ReadSingle());
	}
	static Quaternion ReadQuaternion(BinaryReader binaryReader) {
		return new Quaternion(
			binaryReader.ReadSingle(), binaryReader.ReadSingle(), binaryReader.ReadSingle(), binaryReader.ReadSingle());
	}
	static string ReadString(BinaryReader binaryReader, int length) {
		return System.Text.Encoding.GetEncoding("shift_jis").GetString(binaryReader.ReadBytes(length))
				.TrimEnd('\0').TrimEnd('?').TrimEnd('\0');
	}
	static BezierInterpolator ReadBezierXYXY(BinaryReader binaryReader, int stride) {
		var b = binaryReader.ReadBytes(stride*4);
		return new BezierInterpolator{
			X0 = b[stride*0]/127f, Y0 = b[stride*1]/127f, X1 = b[stride*2]/127f, Y1 = b[stride*3]/127f};
	}
	static BezierInterpolator ReadBezierXXYY(BinaryReader binaryReader, int stride) {
		var b = binaryReader.ReadBytes(stride*4);
		return new BezierInterpolator{
			X0 = b[stride*0]/127f, X1 = b[stride*1]/127f, Y0 = b[stride*2]/127f, Y1 = b[stride*3]/127f};
	}
}
}