using System.Collections.Generic;
using UnityEngine;

namespace VMDMotion {
public class Motion {
	public class BoneCurve {
		public List<VMD.BoneKeyframe> Keyframes = new List<VMD.BoneKeyframe>();
		public void Sample(float frameNumber, out Vector3 position, out Quaternion rotation) {
			Keyframes.BinarySplit(f => f.FrameNumber >= frameNumber, out var prevFrame, out var nextFrame);
			var prevNumber = prevFrame?.FrameNumber ?? 0;
			position = prevFrame?.Position ?? Vector3.zero;
			rotation = prevFrame?.Rotation ?? Quaternion.identity;
			if(nextFrame != null) {
				var x = nextFrame.Interp.X.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var y = nextFrame.Interp.Y.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var z = nextFrame.Interp.Z.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var r = nextFrame.Interp.Rotation.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				position.x = Mathf.Lerp(position.x, nextFrame.Position.x, x);
				position.y = Mathf.Lerp(position.y, nextFrame.Position.y, y);
				position.z = Mathf.Lerp(position.z, nextFrame.Position.z, z);
				rotation = Quaternion.Slerp(rotation, nextFrame.Rotation, r);
			}
		}
		public Vector3 EstimateRotationCenterFromPosition() {
			// goal: least square length(x)-length(x-p)
			Vector3 A0=default, A1=default, A2=default, B=default;
			for(int i=1; i<Keyframes.Count-1; i++) {
				// approximate length(x)-length(x-p) with (dot(x,p)-b) / length(x)
				var p = Keyframes[i].Position;
				var b = p.sqrMagnitude/2;
				var w = Vector3.Distance(p, Keyframes[i-1].Position)+
						Vector3.Distance(p, Keyframes[i+1].Position); // weighted by segment length
				// turn into linear least square: sum_i w_i*(dot(x,p_i)-b_i)^2
				A0 += w * p.x * p;
				A1 += w * p.y * p;
				A2 += w * p.z * p;
				B  += w * b * p;
			}
			// should return zero if all positions are zero
			return new Matrix4x4(A0, A1, A2, new Vector4(0,0,0,1)).inverse.MultiplyVector(B);
		}
	}
	public class FaceCurve {
		public List<VMD.FaceKeyframe> Keyframes = new List<VMD.FaceKeyframe>();
		public float Sample(float frameNumber) {
			Keyframes.BinarySplit(f => f.FrameNumber >= frameNumber, out var prevFrame, out var nextFrame);
			var prevNumber = prevFrame?.FrameNumber ?? 0;
			var weight = prevFrame?.Weight ?? 0;
			if(nextFrame != null) {
				var w = Mathf.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				weight = Mathf.Lerp(weight, nextFrame.Weight, w);
			}
			return weight;
		}
	}
	public class IKCurve {
		public List<VMD.IKKeyframe> Keyframes = new List<VMD.IKKeyframe>();
		public void Sample(float frameNumber, out bool display, out (string,bool)[] ikEnabled) {
			// last keyframe before frame+1 works best in practice
			Keyframes.BinarySplit(f => f.FrameNumber-1 >= frameNumber, out var prevFrame, out var nextFrame);
			display = prevFrame?.Display ?? true;
			ikEnabled = prevFrame?.IKEnabled;
		}
	}
	public class CameraCurve {
		public List<VMD.CameraKeyframe> Keyframes = new List<VMD.CameraKeyframe>();
		public void Sample(float frameNumber, out Vector3 position, out Quaternion rotation,
				out float distance, out float fieldOfView, out bool orthographic) {
			Keyframes.BinarySplit(f => f.FrameNumber >= frameNumber, out var prevFrame, out var nextFrame);
			var prevNumber = prevFrame?.FrameNumber ?? 0;
			position = prevFrame?.Position ?? Vector3.zero;
			var eulerAngles = prevFrame?.EulerAngles ?? Vector3.zero;
			distance = prevFrame?.Distance ?? 0f;
			fieldOfView = prevFrame?.FieldOfView ?? 45f;
			orthographic = prevFrame?.Orthographic ?? false;
			if(nextFrame != null && nextFrame.FrameNumber != prevNumber+1) { // sharp camera change
				var x = nextFrame.Interp.X.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var y = nextFrame.Interp.Y.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var z = nextFrame.Interp.Z.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var r = nextFrame.Interp.EulerAngles.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var d = nextFrame.Interp.Distance.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				var f = nextFrame.Interp.FieldOfView.InverseLerp(prevNumber, nextFrame.FrameNumber, frameNumber);
				position.x = Mathf.Lerp(position.x, nextFrame.Position.x, x);
				position.y = Mathf.Lerp(position.y, nextFrame.Position.y, y);
				position.z = Mathf.Lerp(position.z, nextFrame.Position.z, z);
				eulerAngles = Vector3.Lerp(eulerAngles, nextFrame.EulerAngles, r);
				distance    = Mathf.Lerp(distance, nextFrame.Distance, d);
				fieldOfView = Mathf.Lerp(fieldOfView, nextFrame.FieldOfView, f);
			}
			rotation = Quaternion.Euler(eulerAngles);
		}
	}

	public Dictionary<string, BoneCurve> Bones = new Dictionary<string, BoneCurve>();
	public Dictionary<string, FaceCurve> Faces = new Dictionary<string, FaceCurve>();
	public IKCurve IK = new IKCurve();
	public CameraCurve Camera = new CameraCurve();

	public void AddClip(VMD vmd, int frameOffset=0, bool sort=true) {
		foreach(var keyframe in vmd.BoneKeyframes) {
			if(!Bones.TryGetValue(keyframe.Name, out var curve)) 
				Bones[keyframe.Name] = curve = new BoneCurve();
			keyframe.FrameNumber += frameOffset; // TODO: copy
			curve.Keyframes.Add(keyframe);
		}
		foreach(var keyframe in vmd.FaceKeyframes) {
			if(!Faces.TryGetValue(keyframe.Name, out var curve))
				Faces[keyframe.Name] = curve = new FaceCurve();
			keyframe.FrameNumber += frameOffset; // TODO: copy
			curve.Keyframes.Add(keyframe);
		}
		foreach(var keyframe in vmd.IKKeyframes) {
			keyframe.FrameNumber += frameOffset; // TODO: copy
			IK.Keyframes.Add(keyframe);
		}
		foreach(var keyframe in vmd.CameraKeyframes) {
			keyframe.FrameNumber += frameOffset; // TODO: copy
			Camera.Keyframes.Add(keyframe);
		}
		if(sort) {
			foreach(var curve in Bones.Values)
				curve.Keyframes.Sort((x,y) => x.FrameNumber.CompareTo(y.FrameNumber));
			foreach(var curve in Faces.Values)
				curve.Keyframes.Sort((x,y) => x.FrameNumber.CompareTo(y.FrameNumber));
			IK.Keyframes.Sort((x,y) => x.FrameNumber.CompareTo(y.FrameNumber));
			Camera.Keyframes.Sort((x,y) => x.FrameNumber.CompareTo(y.FrameNumber));
		}
	}
}
static class ListBinarySplitExtension {
	public static void BinarySplit<T>(this List<T> list, System.Predicate<T> pred, out T lastFalse, out T firstTrue) {
		var i = 0;
		var j = list.Count;
		while(i < j) {
			var k = (i+j)/2;
			if(pred(list[k]))
				j = k;
			else
				i = k+1;
		}
		firstTrue = i < list.Count ? list[i] : default;
		lastFalse = i > 0 ? list[i-1] : default;
	}
}
}