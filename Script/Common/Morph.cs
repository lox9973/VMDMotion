using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace VMDMotion {
public class Morph {
	public class BlendShape {
		public SkinnedMeshRenderer target;
		public int index;
		public float weight;
	}
	public Animator animator;
	public Dictionary<string, BlendShape> shapes;

	private SkinnedMeshRenderer[] targets;
	public Morph(Animator animator, IEnumerable<string> shapeNames) {
		var shapeSets = new HashSet<string>(shapeNames);
		shapes = new Dictionary<string, BlendShape>();
		foreach(var smr in animator.GetComponentsInChildren<SkinnedMeshRenderer>()) {
			var mesh = smr.sharedMesh;
			if(!mesh)
				continue;
			for(int i=0; i<mesh.blendShapeCount; i++) {
				var name = mesh.GetBlendShapeName(i);
				name = Regex.Replace(name, @"^\d+[.]", "");
				if(shapeSets.Contains(name) && !shapes.ContainsKey(name)) {
					shapes[name] = new BlendShape{target=smr, index=i};
					Debug.Log($"{name}: {smr} {i}");
				}
			}
		}
	}
	public void ApplyTargets() {
		foreach(var shape in shapes.Values)
			shape.target.SetBlendShapeWeight(shape.index, shape.weight*100f);
	}
}
}