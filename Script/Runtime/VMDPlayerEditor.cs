#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace VMDMotion {
[CustomEditor(typeof(VMDPlayer))]
public class VMDPlayerEditor : Editor {
	SerializedProperty vmdPaths;
	SerializedProperty vmdOffsets;
	SerializedProperty scale;
	void OnEnable() {
		vmdPaths = serializedObject.FindProperty("vmdPaths");
		vmdOffsets = serializedObject.FindProperty("vmdOffsets");
		scale = serializedObject.FindProperty("scale");
	}
	public override void OnInspectorGUI() {
		serializedObject.Update();
		DrawDefaultInspector();
		if(GUILayout.Button("Add VMD file ..")) {
			var openPath = EditorUtility.OpenFilePanel("Select vmd file", "", "vmd");
			if(!string.IsNullOrEmpty(openPath)) {
				var i = vmdPaths.arraySize;
				vmdPaths.InsertArrayElementAtIndex(i);
				vmdPaths.GetArrayElementAtIndex(i).stringValue = openPath;
				vmdOffsets.InsertArrayElementAtIndex(i);
			}
		}
		if(GUILayout.Button("Clear VMD files")) {
			for(int i=vmdPaths.arraySize-1; i>=0; i--) {
				vmdPaths.DeleteArrayElementAtIndex(i);
				vmdOffsets.DeleteArrayElementAtIndex(i);
			}
		}
		if(scale.floatValue <= 0.1f)
			scale.floatValue = EditorGUILayout.Slider(scale.floatValue, 0, 0.1f);
		serializedObject.ApplyModifiedProperties();
	}
}
}
#endif