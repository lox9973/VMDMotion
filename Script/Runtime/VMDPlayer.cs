﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VMDMotion {
using static Standard.BoneNames;
public class VMDPlayer : MonoBehaviour {
[Header("Model")]
[Tooltip("Model to animate")]
	public Animator animator;
[Tooltip("MMD センター bone (optional)")]
	public Transform centerBone;
[Tooltip("MMD 腰 bone (optional)")]
	public Transform waistBone;

[Header("Camera")]
[Tooltip("Camera to animate (optional)")]
	public new Camera camera;

[Header("Motion")]
[Tooltip("VMD file path")]
	public string[] vmdPaths;
[Tooltip("VMD file frame offset")]
	public int[] vmdOffsets;
[Tooltip("Set motion time from scripting")]
	public bool manualUpdateTime;
	[System.NonSerialized] public double time;

[Header("Tweak")]
[Tooltip("Motion scale. 0=auto")]
	public float scale = 0;
[Tooltip("Swap left/right")]
	public bool mirror = false;
[Tooltip("Enable face blendshape")]
	public bool enableShape = true;
[Tooltip("Enable inverse kinematic")]
	public bool enableIK = true;
[Tooltip("Apply rotation of ik goal")]
	public bool enableIKQ = false;
[Tooltip("Arm rotation degree in rest pose (relative to T-pose). 30=A-pose")]
	public float armRestAngle = 30;
	// public Vector3 locomotionScale = Vector3.one; // used to fix bad vmd drifting

	const float FPS = 30f;
	Motion motion;
	Motion.BoneCurve[] boneCurves;
	Skeleton skel;
	Morph morph;
	double startTime;

	void OnEnable() {
		time = 0;
		startTime = Time.time;
		motion = new Motion();
		for(int i=0; i<vmdPaths.Length; i++)
			motion.AddClip(new VMD(vmdPaths[i]), vmdOffsets[i], sort:i==vmdPaths.Length-1);

		foreach(var pair in motion.Bones.ToArray()) {
			var name = Standard.FixBoneName(pair.Key);
			if(name != pair.Key) {
				Debug.Log($"bone rename: {pair.Key} => {name}");
				motion.Bones.Remove(pair.Key);
				motion.Bones.Add(name, pair.Value);
			}
		}
		boneCurves = System.Enum.GetNames(typeof(Standard.BoneNames)).Select(name =>
			motion.Bones.ContainsKey(name) ? motion.Bones[name] : new Motion.BoneCurve()).ToArray();

		// stats
		var maxFrame = motion.Bones.Values.Max(curve => (int?)(curve.Keyframes.LastOrDefault()?.FrameNumber??0))??0;
		Debug.Log("duration: " + $"{maxFrame/FPS:F2}s ({maxFrame})");
		Debug.Log("bone frames: " + string.Join(", ", motion.Bones.Where(pair => pair.Value.Keyframes.Count>2)
			.Select(pair => $"{pair.Key}({pair.Value.Keyframes.Count})")));
		Debug.Log("face frames: " + string.Join(", ", motion.Faces.Where(pair => pair.Value.Keyframes.Count>2)
			.Select(pair => $"{pair.Key}({pair.Value.Keyframes.Count})")));

		if(skel == null) {
			if(!float.IsNaN(armRestAngle))
				ApplyArmPose(-armRestAngle);
			var sourceOverrides = new Dictionary<Standard.BoneNames, Transform>();
			if(centerBone)
				sourceOverrides.Add(センター, centerBone);
			if(waistBone)
				sourceOverrides.Add(腰, waistBone);
			skel = new Skeleton(animator, sourceOverrides);
			morph = new Morph(animator, motion.Faces.Keys);
		}

		DetectScale();
		DetectDisplay();
		ApplyIKDefault();
	}
	float[] scaleOverrides;
	void DetectScale() {
		const float humanScaleToMotionScale = 0.1f/1.204f; // scale=0.1 on Animasa's & TDA's Miku
		if(scale == 0f)
			scale = animator.humanScale * humanScaleToMotionScale;
		else
			Debug.Log($"suggested scale: {animator.humanScale * humanScaleToMotionScale}");

		scaleOverrides = new float[skel.bones.Length];
		foreach(var toeIK in new[]{左つま先ＩＫ, 右つま先ＩＫ}) {
			var curveLocalPos0 = -boneCurves[(int)toeIK].EstimateRotationCenterFromPosition();
			var boneLocalPos0 = skel.bones[(int)toeIK].localPosition0;
			if(curveLocalPos0 != default) { // estimate toe scale from motion
				scaleOverrides[(int)toeIK] = boneLocalPos0.magnitude/curveLocalPos0.magnitude;
				Debug.LogWarning($"override scale: {toeIK}({scaleOverrides[(int)toeIK]:F4}) skeleton={boneLocalPos0*100} motion={curveLocalPos0*100}");
			}
		}
	}
	int firstFrameToDisplay;
	void DetectDisplay() {
		firstFrameToDisplay = 0;
		if(motion.IK.Keyframes.Count > 0)
			return; // display is driven by IK
		const float minSpeedT = 0.02f;
		const float minSpeedQ = 10f; // in degree
		foreach(var boneName in new[]{全ての親, センター, 左足ＩＫ, 右足ＩＫ}) {
			var keyframes = boneCurves[(int)boneName].Keyframes;
			if(keyframes.Count >= 2 && keyframes[0].FrameNumber == 0) {
				var delta = (keyframes[1].FrameNumber - keyframes[0].FrameNumber) / FPS;
				var speedT = Vector3.Distance(keyframes[0].Position, keyframes[1].Position) / delta;
				var speedQ = Quaternion.Angle(keyframes[0].Rotation, keyframes[1].Rotation) / delta;
				var linearT = keyframes[1].Interp.X.IsLinear && keyframes[1].Interp.Y.IsLinear && keyframes[1].Interp.Z.IsLinear;
				var linearQ = keyframes[1].Interp.Rotation.IsLinear;
				// heuristic: detect initial slow motion
				if((linearT && speedT > minSpeedT) || (linearQ && speedQ > minSpeedQ)) {
					firstFrameToDisplay = Mathf.Max(firstFrameToDisplay, keyframes[1].FrameNumber);
					Debug.LogWarning($"skip frame: {keyframes[1].Name}({keyframes[1].FrameNumber}) {speedT} {speedQ}");
				}
			}
		}
	}
	void ApplyIKDefault() {
		lastIKEnabled = null;
		foreach(var (footIK, toeIK) in new[]{(左足ＩＫ, 左つま先ＩＫ), (右足ＩＫ, 右つま先ＩＫ)}) {
			skel.bones[(int)footIK].ikEnabled = !boneCurves[(int)footIK].Keyframes
				.All(f => f.Position.Equals(Vector3.zero) && f.Rotation.Equals(Quaternion.identity));
			skel.bones[(int)toeIK ].ikEnabled = !boneCurves[(int)toeIK ].Keyframes
				.All(f => f.Position.Equals(Vector3.zero) && f.Rotation.Equals(Quaternion.identity));
			skel.bones[(int)footIK].ikCopyQ   = !boneCurves[(int)footIK].Keyframes
				.All(f => f.Rotation.Equals(Quaternion.identity));
			skel.bones[(int)toeIK ].ikCopyQ   = false;
		}
		Debug.Log("ik bone frames: " + string.Join(", ", new[]{左足ＩＫ, 左つま先ＩＫ, 右足ＩＫ, 右つま先ＩＫ}
			.Select(ik => $"{ik}({boneCurves[(int)ik].Keyframes.Count})")));
	}
	
	void Update() {
		if(!manualUpdateTime)
			time = Time.time - startTime;
		UpdateFrame((float)(time*FPS));
	}
	void UpdateFrame(float frame) {
		if(enableShape)
			ApplyFaceFrame(frame);
		ApplyIKFrame(frame);
		ApplyBoneFrame(frame);
		skel.ApplyConstraints(enableIK, enableIKQ);
		skel.ApplyTargets();
		morph.ApplyTargets();
		if(camera)
			ApplyCameraFrame(frame);
	}
	void ApplyFaceFrame(float frame) {
		frame = Mathf.Max(frame, 0);
		foreach(var pair in motion.Faces)
			if(morph.shapes.TryGetValue(pair.Key, out var shape))
				shape.weight = pair.Value.Sample(frame);
	}
	void ApplyBoneFrame(float frame) {
		frame = Mathf.Max(frame, 0);
		foreach(var bone in skel.bones) {
			var index = (int)(mirror ? Standard.MirrorBoneNames[(int)bone.name] : bone.name);
			var curve = boneCurves[index];
			curve.Sample(frame, out var pos, out var rot);
			if(mirror) {
				pos.x *= -1;
				rot.y *= -1;
				rot.z *= -1;
			}
			var scale = scaleOverrides[(int)bone.name];
			if(scale == 0)
				scale = this.scale;
			pos *= scale;
			// if(bone.name == 全ての親 || bone.name == センター || bone.name == 左足ＩＫ || bone.name == 右足ＩＫ)
			// 	pos = Vector3.Scale(pos, locomotionScale);

			bone.transform.localPosition = pos + bone.localPosition0;
			bone.transform.localRotation = rot;
		}
	}
	(string, bool)[] lastIKEnabled;
	void ApplyIKFrame(float frame) {
		motion.IK.Sample(frame, out var display, out var ikEnabled);
		animator.gameObject.SetActive(frame >= firstFrameToDisplay && display);
		if(ikEnabled == lastIKEnabled)
			return; // ikEnabled is idempotent
		lastIKEnabled = ikEnabled;
		if(ikEnabled == null)
			return;
		foreach(var (name, enabled) in ikEnabled)
			if(System.Enum.TryParse<Standard.BoneNames>(name, out var boneName)) {
				if(mirror)
					boneName = Standard.MirrorBoneNames[(int)boneName];
				if(skel.bones[(int)boneName].ikEnabled != enabled)
					Debug.Log($"ik: {boneName}={enabled}");
				skel.bones[(int)boneName].ikEnabled = enabled;
			}
	}
	void ApplyCameraFrame(float frame) {
		motion.Camera.Sample(frame, out var pos, out var rot, out var dist, out var fov, out bool ortho);
		pos += rot * Vector3.forward * dist;
		camera.transform.SetPositionAndRotation(
			skel.root.TransformPoint(pos * scale), skel.root.rotation * rot);
		camera.fieldOfView = fov;
		camera.orthographic = ortho;
	}
	void ApplyArmPose(float degreeToTPose) {
		var bones = new[] {
			(animator.GetBoneTransform(HumanBodyBones.LeftUpperArm), animator.GetBoneTransform(HumanBodyBones.LeftHand)),
			(animator.GetBoneTransform(HumanBodyBones.RightUpperArm), animator.GetBoneTransform(HumanBodyBones.RightHand)),
		};
		foreach(var (upperArm, lowerArm) in bones) {
			var boneVec = animator.transform.InverseTransformVector(lowerArm.position-upperArm.position);
			upperArm.Rotate(animator.transform.forward,
				Mathf.Sign(boneVec.x) * degreeToTPose - Mathf.Rad2Deg * Mathf.Atan(boneVec.y/boneVec.x), Space.World);
		}
	}
}
}