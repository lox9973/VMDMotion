# VMDMotion

VMDMotion is a library to parse and play VMD (Vocaloid Motion Data) animation in Unity runtime on any humanoid avatar, not limited to MMD models.

Tested on Unity 2019.4.29f1.

## Features

The following bone animations (左/右 omitted for brevity) are supported and mapped to Mecanim bones:
- 全ての親, センター, グルーブ, 腰
- 足ＩＫ, つま先ＩＫ (foot/toe IKs are supported)
- 下半身, 足, ひざ, 足首, つま先, 足D, ひざD, 足首D, 足先EX (D bones are supported)
- 上半身, 上半身2, 首, 頭, 目
- 肩P, 肩, 肩C, 腕, 腕捩, ひじ, 手捩, 手首 (twist bones are supported)
- 親指, 人指, 中指, 薬指, 小指

Face animations are mapped to blendshapes by name.

Camera animations are supported. Lights and shadows are not supported.

## Related works

- [UnityVMDPlayer](https://github.com/hobosore/UnityVMDPlayer): main reference of this project. thank you!
- [blender_mmd_tools](https://github.com/powroupi/blender_mmd_tools): a good reference of VMD format
- [VMDMotionGD](https://github.com/EIRTeam/VMDMotionGD): a port to Godot Engine
- [UnityToVMD](https://github.com/vr-voyage/UnityToVMD): export Unity animation to VMD